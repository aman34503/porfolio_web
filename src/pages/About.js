import React from 'react';
import styled from 'styled-components';
import PText from '../components/PText';
import AboutImg from '../assets/images/about-page-img.png';
import AboutInfoItem from '../components/AboutInfoItem';
import ContactBanner from '../components/ContactBanner';

const AboutPageStyles = styled.div`
  padding: 20rem 0 10rem 0;
  .button {
    font-size: 5 rem;
  }
  .top-section {
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 2rem;
  }
  .left {
    flex: 30;
  }
  .right {
    flex: 20;
  }
  .about__subheading {
    font-size: 2.2rem;
    margin-bottom: 2rem;
    span {
      background-color: var(--deep-dark);
      padding: 0.5rem;
      border-radius: 8px;
    }
  }
  .about__heading {
    font-size: 3.6rem;
    margin-bottom: 3rem;
  }
  .about__info {
    margin-bottom: 4rem;
    .para {
      max-width: 100%;
    }
  }
  .right {
    img {
      border: 4px solid var(--gray-1);
    }
  }
  .about__info__items {
    margin-top: 15rem;
  }
  .about__info__item {
    margin-bottom: 10rem;
  }
  .about__info__heading {
    font-size: 3.6rem;
    text-transform: uppercase;
  }
  @media only screen and (max-width: 768px) {
    padding: 10rem 0;
    .top-section {
      flex-direction: column;
      gap: 5rem;
    }
    .about__subheading {
      font-size: 1.8rem;
    }
    .about__heading {
      font-size: 2.8rem;
    }
    .about__info__heading {
      font-size: 3rem;
    }
  }
`;

export default function About() {
  return (
    <>
      <AboutPageStyles>
        <div className="container">
          <div className="top-section">
            <div className="left">
              <p className="about__subheading">
                Hi, I am <span>Aman Shrivastava</span>
              </p>
              <h2 className="about__heading">just a Guy who loves Coding</h2>
              <div className="about__info">
                <PText>
                  I am from Gwalior, MadhyaPradesh, India. A Heritage city of
                  Madhyapradesh. Since my childhood, i love to Solve Problmes in
                  unique Ways. I always try to design stuff with my unique point
                  of view. I also love to create things that can be usefull to
                  others.
                  <br /> <br />
                  As being from a computer science student my interests lies in
                  programming and learning new technical stuffs. I am an
                  enthusiastic Competitive programming learner and believe in
                  working hard and enjoying life aswell. Coding is also an art
                  for me. I love it and now I have the opportunity to design
                  along with the coding. I find it really interesting and I
                  enjoyed the process a lot.
                  <br />
                  <br />
                  My vision is to make the world a better place. Now almost
                  everything is becoming better than ever. It is time for us to
                  create more good stuff that helps the world to become a better
                  place.
                  <br />
                  <br />I love designing web frontend websites with good catchy
                  styles and theme Apart from technical things ,I am also
                  interested in gaming , watching technical blogs , sports,
                  playing Guitar. My hobbies are listening to HipHop-Banger,
                  playing Chess. playing volley ball and Cricket during my
                  leisure hours .
                </PText>
              </div>
              <div className="button">
                <a
                  variant="primary"
                  size="lg"
                  href=" https://drive.google.com/file/d/1pKdhoaUChPrTtJyWrq3I4hyIoAMRZfXD/view?usp=sharing"
                  className="button"
                >
                  <h1 className="button"> click -> Download CV</h1>
                </a>
              </div>
            </div>
            <div className="right">
              <img src={AboutImg} alt="me" />
            </div>
          </div>
          <div className="about__info__items">
            <div className="about__info__item">
              <h1 className="about__info__heading">Education</h1>

              <AboutInfoItem
                title="School"
                items={['Delhi Public Academy, Gwalior']}
              />
              <AboutInfoItem
                title="College"
                items={['Acropolis Technical Campus (2019-2023)']}
              />
              <AboutInfoItem
                title="University"
                items={['Rajiv Gandhi Proudyogiki Vishwavidyalaya']}
              />
            </div>
            <div className="about__info__item">
              <h1 className="about__info__heading">My Skills</h1>

              <AboutInfoItem
                title="FrontEnd"
                items={['HTML', 'CSS', 'JavaScript', 'C++', 'Python']}
              />
              <AboutInfoItem title="BackEnd" items={['Node', 'Express']} />
              <AboutInfoItem
                title="DATABASE"
                items={['MongoDB', 'Mysql', 'Postgre sql']}
              />
              <AboutInfoItem
                title="Frameworks"
                items={['React', 'Bootstrap', 'Flask']}
              />
              <AboutInfoItem
                title="Libraries"
                items={['Numpy', 'Pandas', 'OpenCV', 'Matplotlib']}
              />
              <AboutInfoItem
                title="Other"
                items={['Git', 'Shell Scripting', 'Gatsby', 'Heroku']}
              />
            </div>
          </div>
        </div>
        <ContactBanner />
      </AboutPageStyles>
    </>
  );
}
