import React from 'react';
import styled from 'styled-components';
import FooterCol from './FooterCol';
import PText from './PText';

const FooterStyle = styled.div`
  background-color: var(--deep-dark);
  padding-top: 10rem;
  .container {
    display: flex;
    gap: 0rem;
  }
  .footer__col1 {
    flex: 2;
  }
  .footer__col2,
  .footer__col3,
  .footer__col4 {
    flex: 1;
  }
  .footer__col1__title {
    font-size: 3.5rem;
    margin-bottom: 1rem;
  }
  .copyright {
    background-color: var(--dark-bg);
    text-align: left;
    padding: rem 0;
    margin-top: 2rem;
    .para {
      margin-left: 1;
      margin right: 4;
    }
  }
  @media only screen and (max-width: 768px) {
    .container {
      flex-direction: column;
      gap: 0rem;
      & > div {
        margin-top: 5rem;
      }
    }
    .footer__col1 .para {
      max-width: 100%;
    }
    .copyright {
      .container {
        div {
          margin-top: 0;
        }
      }
    }
  }
`;

export default function Footer() {
  return (
    <FooterStyle>
      <div className="container">
        <div className="footer__col1">
          <h1 className="footer__col1__title">Aman Shrivastava</h1>
          <PText> Make it work, make it right, make it fast.</PText>
        </div>
        <div className="footer__col2">
          <FooterCol
            heading="Important Links"
            links={[
              {
                title: 'Home',
                path: '/',
                type: 'Link',
              },
              {
                type: 'Link',
                title: 'About',
                path: '/about',
              },
              {
                type: 'Link',
                title: 'Projects',
                path: '/projects',
              },
              {
                type: 'Link',
                title: 'Contact',
                path: '/contact',
              },
            ]}
          />
        </div>
        <div className="footer__col3">
          <FooterCol
            heading="Contact Info"
            links={[
              {
                title: '+916266038184',
                path: 'tel:+91 6266038184',
              },
              {
                title: 'aman34503@gmail.com',
                path: 'mailto:aman34503@gmail.com',
              },
              {
                title: 'Radhakrishnapuri, Airport road Gwalior',
                path: 'http://google.com/maps',
              },
            ]}
          />
        </div>
        <div className="footer__col4">
          <FooterCol
            heading="Social Links"
            links={[
              {
                title: 'github',
                path: 'https://github.com/aman34503',
              },
              {
                title: 'Twitter',
                path: 'http://twitter.com',
              },
              {
                title: 'linkedin',
                path: 'https://www.linkedin.com/in/aman-shrivastava04/',
              },
            ]}
          />
        </div>
      </div>
      <div className="copyright">
        <div className="container">
          <PText>
            © 2021 - Aman Shrivastava | Designed By{' '}
            <a target="_blank" rel="noreferrer" href="http://aman34503.com">
              Aman Shrivastava
            </a>{' '}
          </PText>
        </div>
      </div>
    </FooterStyle>
  );
}
