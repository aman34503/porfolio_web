import React from 'react';
import styled from 'styled-components';
import HeroImg from '../assets/images/hero.png';
import Button from './Button';
import SocialMediaArrow from '../assets/images/social-media-arrow.svg';
import ScrollDownArrow from '../assets/images/scroll-down-arrow.svg';
import PText from './PText';

const HeroStyles = styled.div`
  .hero {
    height: 100vh;
    min-height: 1000px;
    width: 100%;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
  }
  .hero__heading {
    font-size: 2rem;
    margin-bottom: -4rem;
    position: relative;
    span {
      display: inline-block;
      width: 100%;
    }
    .hero__name {
      font-family: 'Montserrat SemiBold';
      font-size: 7rem;
      color: var(--white);
    }
  }
  .hero__img {
    max-width: 1000px;
    width: 50%;
    height: 600px;
    margin: 0 auto;
    border: 1px double var(--gray-1);
  }
  .hero__info {
    margin-top: -18rem;
  }
  .hero__social,
  .hero__scrollDown {
    display: flex;
    flex-direction: column;
    gap: 2rem;
    position: absolute;
    bottom: 0px;
    top: 52vh;
    right: 340vh;
    width: 50px;
  }
  .hero__social {
    left: 400px;
  }

  .hero__scrollDown {
    right: 500px;
  }
  .hero__social__indicator,
  .hero__scrollDown {
    width: 50px;
    p {
      font-size: 1.6rem;
      transform: translateY(0px) rotate(0deg);
      letter-spacing: 1.7rem;
      text-transform: uppercase;
    }
    img {
      max-height: 2rem;
      width: 166px;
      margin: 0 auto;
      object-fit: contain;
    }
  }
  .hero__scrollDown {
    img {
      max-height: 40px;
    }
  }
  .hero__social__profile {
    ul {
      li {
        font-size: 15px;
        margin-left: 800px;
        margin-bottom: 1rem;
        position: relative;
        left: 70px;
        top: -40px;
        a {
          display: inline-block;
          transform: rotate(0deg);
          margin-top: 0%;
          margin-bottom: 2rem;
          font-size: 18px;
          letter-spacing: 2px;
        }
      }
    }
  }
  .hero__social__text {
    ul {
      li {
        margin-bottom: 1rem;
        a {
          display: inline-block;
          font-size: 2rem;
          transform: rotate(0deg);
          letter-spacing: 5px;
          margin-bottom: 1rem;
          margin-top: 1rem;
        }
      }
    }
  }
  @media only screen and (max-width: 768px) {
    .hero {
      min-height: 750px;
    }
    .hero__heading {
      font-size: 1.4rem;
      margin-bottom: -3rem;
      .hero__name {
        font-size: 4.5rem;
      }
    }
    .hero__img {
      height: 300px;
    }
    .hero__info {
      margin-top: 3rem;
    }
    .hero__social {
      left: 0px;
      bottom: -15%;
      width: 20px;
      .hero__social__indicator {
        width: 20px;
        p {
          font-size: 1.2rem;
        }
        img {
          max-height: 22px;
        }
      }
      .hero__social__profile {
        ul {
          li {
            a {
              font-size: 1.2rem;
              margin-bottom: 1rem;
            }
          }
        }
      }
      .hero__social__text {
        ul {
          li {
            a {
              font-size: 1.2rem;
              margin-bottom: 1rem;
            }
          }
        }
      }
    }
    .hero__scrollDown {
      right: 04px;
      width: 20px;
      gap: 1rem;
      p {
        font-size: 1.3rem;
      }
    }
  }
`;

export default function HeroSection() {
  return (
    <HeroStyles>
      <div className="hero">
        <div className="container">
          <h1 className="hero__heading">
            <span>Hello, This is</span>
            <span className="hero__name">Aman Shrivastava</span>
          </h1>
          <div className="hero__img">
            <img src={HeroImg} alt="" />
          </div>
          <div className="hero__info">
            <PText />
            <Button btnText="see my works ? " btnLink="/projects" />
          </div>
          <div className="hero__social">
            <div className="hero__social__indicator">
              <p>Follow</p>
              <img src={SocialMediaArrow} alt="icon" />
            </div>
            <div className="hero__social__text">
              <ul>
                <li>
                  <a
                    href="https://github.com/aman34503"
                    target="_blank"
                    rel="noreferrer"
                  >
                    github
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.linkedin.com/in/aman-shrivastava04/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    linkedin
                  </a>
                </li>
                <li>
                  <a
                    href="https://amanshri345.blogspot.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Blogger
                  </a>
                </li>
                <li>
                  <a
                    href="https://twitter.com/Amanscasm"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Twitter
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="hero__scrollDown">
            <p>Coding Profiles</p>
            <img src={ScrollDownArrow} alt="ScrollDown Arrow" />
          </div>
          <div className="hero__social__profile">
            <ul>
              <li>
                <a
                  href="https://auth.geeksforgeeks.org/user/aman34503/profile"
                  target="_blank"
                  rel="noreferrer"
                >
                  GFG
                </a>
              </li>
              <li>
                <a
                  href=" https://leetcode.com/aman34503/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Leetcode
                </a>
              </li>{' '}
              <li>
                <a
                  href="https://www.codechef.com/users/amanshri0408"
                  target="_blank"
                  rel="noreferrer"
                >
                  Codechef
                </a>
              </li>{' '}
              <li>
                <a
                  href="https://www.hackerrank.com/aman011"
                  target="_blank"
                  rel="noreferrer"
                >
                  Hackerrank
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </HeroStyles>
  );
}
