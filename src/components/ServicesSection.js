import React from 'react';
import { MdDesktopMac, MdCode, MdPhonelinkSetup } from 'react-icons/md';
import styled from 'styled-components';
import SectionTitle from './SectionTitle';
import ServicesSectionItem from './ServicesSectionItem';

const ServicesItemsStyles = styled.div`
  padding: 10rem 10;
  .services__allItems {
    display: flex;
    gap: 25rem;
    justify-content: center;
    margin-top: 5rem;
  }
  @media only screen and (max-width: 768px) {
    .services__allItems {
      flex-direction: row;
      max-width: 350px;
      margin: 0 auto;
      margin-top: 5rem;
      gap: 15rem;
    }
  }
`;

export default function ServicesSection() {
  return (
    <ServicesItemsStyles>
      <div className="container">
        <SectionTitle subheading=" " heading="What I DO" />
        <div className="services__allItems">
          <ServicesSectionItem
            icon={<MdDesktopMac />}
            title="Web devlopement"
            desc="  I have always been passionate about web build design and development . Web Development isn't just what a product looks like and feels like on the outside. During the development phase the designs undertaken encompasses the internal functionality of a product as well as the overall user experience. I strive to design interfaces and experiences that people can enjoy on all digital mediums."
          />
          <ServicesSectionItem
            icon={<MdCode />}
            title="Competitive programming"
            desc="
            Being a Computer Science student and having a string hold in programming languages like Python and C++ . I'm passionate about competitive programming as it helps to increase our problem solving skills and helps us bring codes into real worls practise. I am striving to be a good programmer and learning it day by day , I also love to keep my code simple so as to keep it understandable for every programming enthusiast.
          "
          />
        </div>
      </div>
    </ServicesItemsStyles>
  );
}
