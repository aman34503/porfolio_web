import { v4 as uuidv4 } from 'uuid';
import ProjectImg from '../images/projectImg.png';
import UTrackerImg from '../images/utracker.jpg';
import GreenCtgImg from '../images/greenctg.jpg';
import CoinTrackerImg from '../images/cointracker.jpg';
import CavinImg from '../images/cavinimg.jpg';

const projects = [
  {
    id: uuidv4(),
    name: ' Covid Tracker ',
    desc:
      'An application to track Covid 19 data of all around the world. I developed the Android app with the help of Volley Library and Api Fetch',
    img: UTrackerImg,
  },
  {
    id: uuidv4(),
    name: 'Chemistry Periodic Table Ui',
    desc:
      'It is using the chemical-element-visualisation Web Component built with html and css',
    img: GreenCtgImg,
  },
  {
    id: uuidv4(),
    name: 'Stock Price Tracker',
    desc:
      '	Using StreamLit Library in python	For Data visulization use matplotlib library in Python',
    img: ProjectImg,
  },
  {
    id: uuidv4(),
    name: 'Geeks Resources',
    desc:
      'A Website for Tech Student where they Can find All the Resources which are Available in Internet at one place. The Website is made using Html Css and Bootstrap',
    img: CavinImg,
  },
  {
    id: uuidv4(),
    name: 'My Travel Memories',
    desc:
      'A website which Follow Basically a Crud Operations. it is MERN based Application where user Post their Event and Everyone Checks Out their Post and LInk and Comment on it.',
    img: CoinTrackerImg,
  },
];

export default projects;
