# porfolio_website

A portfolio for a web designer. We used React js to make this portfolio. A clean design with full responsiveness.

## Project Requirement

 HTML, CSS
 JavaScript
 React Basic (optional)


- React
- React Hooks
- Styled Components
- Swiper js
- React Transition Group
- Smooth Scrollbar
- React Icons
- React Router Dom



After getting the starter files, you need to go the file directory and run

```shell
npm install
```

and after that start the live server.

```shell
npm start
```
